﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using StackExchange.Redis;
using Newtonsoft.Json;

namespace tryAzureRedis
{
    class Program
    {
        static void Main(string[] args)
        {

            //https://docs.microsoft.com/en-us/azure/redis-cache/cache-dotnet-how-to-use-azure-redis-cache

            ConnectionMultiplexer conn = ConnectionMultiplexer.Connect("TryRedis.redis.cache.windows.net:6380,password=142fLuaNTfjjV7l4LwIgd9kq2kDa7bFMf85S2YBinGk=,ssl=True,abortConnect=False");
            //EndPoint myEndPoint = conn.GetEndPoints().First();
            //IServer server = conn.GetServer(myEndPoint);
            //預設會有16個Redis DB (0-15) 用第3個測試看看
            IDatabase cache = conn.GetDatabase(3);
            //cache.StringSet(key, value, expire, When.NotExists);
            #region 測試1

            Console.WriteLine("測試1");
            //寫入cache 3秒
            cache.StringSet("test1", "Hello", TimeSpan.FromSeconds(3));
            //讀出
            var test1 = cache.StringGet("test1");
            Console.WriteLine(string.Format("test1:{0}", test1));

            //When.NotExists不存在才寫入cache
            cache.StringSet("test1", "Hello123", TimeSpan.FromSeconds(3), When.NotExists);
            test1 = cache.StringGet("test1");
            Console.WriteLine(string.Format("test1(3秒內):{0}", test1));//結果不會被覆蓋


            var task1 = Task.Factory.StartNew(() =>
            {
                //測試3秒過後
                Thread.Sleep(3000);
                cache.StringSet("test1", "Hello456", null, When.NotExists);
                test1 = cache.StringGet("test1");
                Console.WriteLine(string.Format("test1(3秒後):{0}", test1));//結果3秒後沒有cache了就會被覆蓋
            });
            task1.Wait();
            #endregion

            #region 測試2

            Console.WriteLine("測試2");
            //寫入cache 3秒
            cache.StringSet("test2", "Hey", TimeSpan.FromSeconds(3));

            //讀出
            var test2 = cache.StringGet("test2");
            Console.WriteLine(string.Format("test2:{0}", test2));

            //When.NotExists不存在才寫入cache
            cache.StringSet("test2", "Hey123", TimeSpan.FromSeconds(3), When.Always);
            test2 = cache.StringGet("test2");
            Console.WriteLine(string.Format("test2(3秒內):{0}", test2));//結果一律被覆蓋
            #endregion

            #region 測試3

            Console.WriteLine("測試3");
            //When.Exists已存在才寫入
            cache.StringSet("test3", "Hi", null, When.Exists);

            //讀出
            var test3 = cache.StringGet("test3");
            Console.WriteLine(string.Format("test3:{0}", test3));//test3這個KEY不存在所以沒有值

            //接著先創造一個test3
            cache.StringSet("test3", "Hi");
            test3 = cache.StringGet("test3");
            Console.WriteLine(string.Format("test3(存在):{0}", test3));
            //已存在就會被覆蓋
            cache.StringSet("test3", "Hi123", null, When.Exists);
            test3 = cache.StringGet("test3");
            Console.WriteLine(string.Format("test3(已存在就會被覆蓋):{0}", test3));
            #endregion

            #region 測試4
            Console.WriteLine("測試4");
            testSample_beverage _beverage = new testSample_beverage();
            string jsonStr = JsonConvert.SerializeObject(_beverage);
            //寫入一個json字串
            cache.StringSet("test4", jsonStr);
            //取出json字串
            var test4 = cache.StringGet("test4");
            Console.WriteLine(test4);
            #endregion

            Console.Read();
            
        }
    }

    class testSample_beverage
    {
        public string b_Name { get; set; }
        public int b_price { get; set; }
        public testSample_beverage()
        {
            b_Name = "紅茶";
            b_price = 20;
        }
    }
}
